import { BrowserRouter, Routes, Route } from 'react-router-dom';
import FormularioEstudiantes from './components/FormularioEstudiantes';
import ListadoEstudiantes from './components/ListadoEstudiantes';


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path='/' element={<ListadoEstudiantes />} />
        <Route path='/formulario-estudiantes' element={<FormularioEstudiantes />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
