import axios from "axios"
import Swal from "sweetalert2"


const Estudiantes = ({ estudiante, actualizarEstudiantes, setActualizarEstudiantes, handleOpenModal, setDataModal }) => {

    const url = " http://localhost:8090/estudiantes"

    const handleDelete = async () => {

        Swal.fire({
            title: `Estas seguro de eliminar a ${estudiante.nombre}?`,
            text: "Esta accion no se puede deshacer!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "Cancelar",
            confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`${url}/${estudiante.id}`).then((response) => {

                    if (response.status === 200) {
                        Swal.fire(
                            'Eliminado!',
                            'Estudiante eliminado correctamente!',
                            'success'
                        )
                        setActualizarEstudiantes(!actualizarEstudiantes)
                    } else {
                        Swal.fire(
                            'Error!',
                            'Hubo un problema al eliminar al estudiante!',
                            'error'
                        )
                    }
                })
            }
        })
    }


    const handleEdit = () => {
        handleOpenModal()
        setDataModal(estudiante)
    }

    return (
        <tr>
            <td>{estudiante.nombre}</td>
            <td>{estudiante.apellido}</td>
            <td>{estudiante.email}</td>
            <td>{estudiante.status}</td>
            <td>
                <button
                    type="button"
                    className="btn btn-danger mr-2"
                    onClick={handleDelete}>
                    Eliminar
                </button>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={handleEdit}>
                    Editar
                </button>
            </td>

        </tr>
    )
}


export default Estudiantes