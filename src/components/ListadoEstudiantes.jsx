import { Fragment, useEffect, useState } from "react"
import { Link } from "react-router-dom"
import axios from "axios"
import Estudiantes from "./Estudiantes"
import { Form, Modal } from "react-bootstrap"
import Swal from "sweetalert2"



const ListadoEstudiantes = () => {


    const url = "  http://localhost:8090/estudiantes"


    const [estudiantes, setEstudiantes] = useState([])
    const [actualizarEstudiantes, setActualizarEstudiantes] = useState(false)
    const [modal, setModal] = useState(false)
    const [dataModal, setDataModal] = useState({})

    const handleCloseModal = () => { setModal(false) }
    const handleOpenModal = () => { setModal(true) }

    const handleChangeModal = ({ target }) => {
        setDataModal({
            ...dataModal,
            [target.name]: target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const response = await axios.put(`${url}/${dataModal.id}`, dataModal)

        if (response.status === 200) {
            Swal.fire(
                'Actualizado!',
                'Estudiante actualizado correctamente!',
                'success'
            )
            handleCloseModal()
            setActualizarEstudiantes(!actualizarEstudiantes)
        } else {
            Swal.fire(
                'Error!',
                'Hubo un problema al actualizar los datos!',
                'error'
            )
        }
    }

    const getData = async () => {
        const response = await axios.get(url)
        return response
    }
    useEffect(() => {
        getData().then((response) => {
            setEstudiantes(response.data)

        })
    }, [actualizarEstudiantes])



    return (
        <Fragment>
            <h1 className="text-center mt-3">
                Listado de Estudiantes
            </h1>
            <Link to={`/formulario-estudiantes`}
                className="btn btn-primary mb-1 ml-2"
            >
                Nuevo Estudiante
            </Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        estudiantes.length === 0 ? "no hay estudiantes" : (
                            estudiantes.map((estudiante, id) => (
                                <Estudiantes
                                    key={id}
                                    estudiante={estudiante}
                                    setActualizarEstudiantes={setActualizarEstudiantes}
                                    actualizarEstudiantes={actualizarEstudiantes}
                                    handleCloseModal={handleCloseModal}
                                    handleOpenModal={handleOpenModal}
                                    setDataModal={setDataModal}
                                />
                            ))
                        )}

                </tbody>
            </table>

            <Modal show={modal} onHide={handleCloseModal}>
                <Modal.Header>
                    <Modal.Title>
                        Actualizar datos
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form
                        onSubmit={handleSubmit}>
                        <div className="form-group">
                            <Form.Label>Nombre</Form.Label>
                            <input type="text"
                                className="form-control"
                                placeholder="Nombre del estudiante"
                                name="nombre"
                                value={dataModal.nombre}
                                required
                                onChange={handleChangeModal}>
                            </input>
                        </div>

                        <div className="form-group">
                            <label htmlFor="apellido">Apellido</label>
                            <input type="text"
                                className="form-control"
                                placeholder="Apellido del estudiante"
                                name="apellido"
                                value={dataModal.apellido}
                                required
                                onChange={handleChangeModal}
                            >
                            </input>
                        </div>

                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email"
                                className="form-control"
                                placeholder="Email del estudiante"
                                name="email"
                                value={dataModal.email}
                                required
                                onChange={handleChangeModal}
                            >
                            </input>
                        </div>

                        <div className="form-group col-md-4">
                            <label htmlFor="inputState">Status</label>
                            <select className="form-control"
                                name="status"
                                required
                                value={dataModal.status}
                                onChange={handleChangeModal}>
                                <option selected>Seleccionar</option>
                                <option value="Activo">Activo</option>
                                <option value="Inactivo">Inactivo</option>
                            </select>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-secondary" type="button" onClick={handleCloseModal}>Cancelar</button>
                    <button className="btn btn-success" type="submit" onClick={handleSubmit} >Guardar cambios</button>
                </Modal.Footer>
            </Modal>

        </Fragment>
    )
}


export default ListadoEstudiantes