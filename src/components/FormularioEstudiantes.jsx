import axios from "axios"
import { useState } from "react"
import Swal from "sweetalert2"
import { useNavigate } from "react-router-dom"



const FormularioEstudiantes = () => {

    const [data, setData] = useState({ nombre: "", apellido: "", email: "", status: "" })

    const navigate = useNavigate()

    const url = " http://localhost:8090/estudiantes"

    const handleChange = ({ target }) => {
        setData({
            ...data,
            [target.name]: target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const response = await axios.post(url, data)
        if (response.status === 201) {
            Swal.fire(
                'Agregado!',
                'Estudiante agregado correctamente!',
                'success'
            )
            navigate("/")
        }
    }

    return (
        <div className="container">
            <h2 className="mt-3">Formulario Nuevo Estudiante</h2>
            <form
                onSubmit={handleSubmit}>

                <div className="form-group">
                    <label htmlFor="nombre">Nombre</label>
                    <input type="text"
                        className="form-control"
                        placeholder="Nombre del estudiante"
                        name="nombre"
                        value={data.nombre}
                        required
                        onChange={handleChange}>
                    </input>
                </div>

                <div className="form-group">
                    <label htmlFor="apellido">Apellido</label>
                    <input type="text"
                        className="form-control"
                        placeholder="Apellido del estudiante"
                        name="apellido"
                        value={data.apellido}
                        required
                        onChange={handleChange}
                    >
                    </input>
                </div>

                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email"
                        className="form-control"
                        placeholder="Email del estudiante"
                        name="email"
                        value={data.email}
                        required
                        onChange={handleChange}
                    >
                    </input>
                </div>

                <div className="form-group col-md-4">
                    <label htmlFor="inputState">Status</label>
                    <select className="form-control"
                        name="status"
                        required
                        value={data.status}
                        onChange={handleChange}>
                        <option selected>Seleccionar</option>
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Agregar Estudiante</button>
            </form>
        </div>
    )
}


export default FormularioEstudiantes